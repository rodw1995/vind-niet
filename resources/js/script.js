var searchApp = angular.module('searchApp', ['ngRoute', 'ngSanitize']);

searchApp.constant('CONFIG', {
    APIHost: 'api'
});

// Set up angular routes
searchApp.config(function($routeProvider, $locationProvider) {
    $routeProvider

        .when('/', {
            templateUrl : 'partials/index.html'
        })
        .when('/results/:queryString', {
            templateUrl : 'partials/results.html'
        })
        .when('/about', {
            templateUrl : 'partials/about.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);
});

searchApp.factory('data', function () {
    return { query: '' };
});

searchApp.run(function($rootScope, $location, data) {

    $rootScope.search = function() {

        if (data.query) {
            $location.path('/results/' + encodeURIComponent(data.query));
        }
    }
});

searchApp.controller('indexController', function($scope, data, $rootScope, $http, CONFIG) {

    $scope.data = data;
    // Reset queryString
    $scope.data.query = '';

    $scope.gamble = function() {

        $http.get(CONFIG.APIHost + '/gamble.json').
            then(function(response) {
                $scope.data.query = response.data.query_string;

                $rootScope.search();
            }, function(response) {

            });
    };

});

searchApp.controller('resultController', function($scope, data, $routeParams, $http, transformRequestAsFormPost, CONFIG) {

    var noResultsFooterText = "Geen resultaten, maak je geen zorgen dat is de bedoeling.";

    $scope.data = data;
    $scope.data.query = decodeURIComponent($routeParams.queryString);

    $scope.noResults = function() {

        if ($scope.results === undefined) {
            return false;
        }

        return $scope.results.length === 0;
    };

    var postRequest = $http({
        method: 'post',
        url: CONFIG.APIHost + '/searches.json',
        transformRequest: transformRequestAsFormPost,
        data: {
            queryString: $scope.data.query
        },
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });

    postRequest.success(function(response) {

        $scope.results = response.results;

        if ($scope.results.length == 0) {
            $scope.footerText = noResultsFooterText;
        } else {
            $scope.footerText = "Wow, je hebt iets gevonden!";
        }
    });

    postRequest.error(function(response) {
        console.log(response);

        // Create an empty results array
        $scope.results = [];
        $scope.footerText = noResultsFooterText;
    });
});

searchApp.directive('myNamedRoute',['CONFIG',
   function(CONFIG) {

       var routesMapping = {
           'about': '/about',
           'home': '/',
           'api_doc': CONFIG.APIHost + '/doc',
           'git': 'https://github.com/rodw1995/vind-niet'
       };

       return {
           restrict: "A",

           link: function(scope, element, attrs) {
               var routeName = attrs.myNamedRoute;

               // Default url if it is not found in de mapping
               var url = '/' + routeName;

               if (routesMapping.hasOwnProperty(routeName)) {
                   url = routesMapping[routeName];
               }

               element.attr('href', url);
           }
       }
   }

]);

searchApp.directive("myColorize", [
    function() {
        return {
            restrict: "A",

            link: function(scope, element, attrs) {

                var message = attrs.myColorize;
                var colors = new Array("#F16745", "#93648D", "#4CC3D9", "#FFC65D");
                for (var i = 0; i < message.length; i++)
                    element.append("<span style=\"color:" + colors[(i % colors.length)] + ";\">" + message[i] + "</span>");
            }
        };
    }
]);

searchApp.directive('myRemovePlaceholder', [
    function() {
        return {
            restrict: "A",

            link: function(scope, element, attrs) {

                function removePlaceholder() {
                    element.removeAttr('placeholder');
                }

                if (attrs.removeOnWidth >= window.innerWidth) {
                    removePlaceholder();
                }

                window.addEventListener("resize", removePlaceholder);
            }
        }
    }
]);

searchApp.directive('myMarginTop', [
   function() {
       return {
           restrict: "A",

           link: function(scope, element, attrs) {
               function searchBoxMargin() {

                   var percentage = 24;

                   if (window.innerWidth <= 445) {
                       percentage = 5;
                   }

                   var topMargin = window.innerHeight / 100 * percentage;
                   element.css({ "marginTop": topMargin + 'px'});
               }

               searchBoxMargin();

               window.addEventListener("resize", searchBoxMargin);
           }
       }
   }
]);

searchApp.directive("myKeyPress", [
    function() {
        return {
            restrict: "A",

            link: function(scope, element, attrs) {
                element.bind("keypress", function(event) {
                    var keyCode = event.which || event.keyCode;

                    if (keyCode == attrs.code) {
                        scope.$apply(function() {
                            scope.$eval(attrs.myKeyPress, {$event: event});
                        });
                    }
                });
            }
        };
    }
]);

searchApp.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

// I provide a request-transformation method that is used to prepare the outgoing
// request as a FORM post instead of a JSON packet.
searchApp.factory(
    "transformRequestAsFormPost",
    function() {
        // I prepare the request data for the form post.
        function transformRequest( data, getHeaders ) {
            var headers = getHeaders();
            headers[ "Content-type" ] = "application/x-www-form-urlencoded; charset=utf-8";
            return( serializeData( data ) );
        }
        // Return the factory value.
        return( transformRequest );
        // ---
        // PRVIATE METHODS.
        // ---
        // I serialize the given Object into a key-value pair string. This
        // method expects an object and will default to the toString() method.
        // --
        // NOTE: This is an atered version of the jQuery.param() method which
        // will serialize a data collection for Form posting.
        // --
        // https://github.com/jquery/jquery/blob/master/src/serialize.js#L45
        function serializeData( data ) {
            // If this is not an object, defer to native stringification.
            if ( ! angular.isObject( data ) ) {
                return( ( data == null ) ? "" : data.toString() );
            }
            var buffer = [];
            // Serialize each key in the object.
            for ( var name in data ) {
                if ( ! data.hasOwnProperty( name ) ) {
                    continue;
                }
                var value = data[ name ];
                buffer.push(
                    encodeURIComponent( name ) +
                    "=" +
                    encodeURIComponent( ( value == null ) ? "" : value )
                );
            }
            // Serialize the buffer and clean it up for transportation.
            var source = buffer
                    .join( "&" )
                    .replace( /%20/g, "+" )
                ;
            return( source );
        }
    }
);