<?php

use Rodw\SearchEngineBundle\Commands\SaveSearchRequestCommandHandler;

class SaveSearchRequestCommandHandlerTest extends \PHPUnit_Framework_TestCase
{
    private $testQueryString;
    private $requestRepository;
    private $command;
    private $searchRepository;
    private $recorder;

    protected function setUp()
    {
        $this->testQueryString = 'test search string';

        // Mock the requestRepository
        $this->requestRepository = $this->getMock('\Rodw\SearchEngineBundle\Repository\Contracts\RequestRepositoryInterface');

        $this->requestRepository->expects($this->once())
            ->method('save');

        // Mock the searchRepository
        $this->searchRepository = $this->getMock('\Rodw\SearchEngineBundle\Repository\Contracts\SearchRepositoryInterface');

        // Mock the command
        $this->command = $this->getMockBuilder('\Rodw\SearchEngineBundle\Commands\SaveSearchRequestCommand')
            ->disableOriginalConstructor()
            ->getMock();

        // Mock RecordsMessages
        $this->recorder = $this->getMock('\SimpleBus\Message\Recorder\RecordsMessages');

        $this->recorder->expects($this->once())
            ->method('record');
    }

    protected function tearDown()
    {
    }

    /**
     * Test that we can save a new unique Search Request
     */
    /** @test */
    public function Save_A_Unique_Search_Request()
    {
        // Mock the searchRepository
        $this->searchRepository->expects($this->once())
            ->method('findByQuery')
            ->with($this->equalTo($this->testQueryString))
            ->will($this->returnValue(null));

        // Mock command
        $this->command->expects($this->exactly(2))
            ->method('queryString')
            ->will($this->returnValue($this->testQueryString));

        // Make the call to the commandHandler class
        $commandHandler = new SaveSearchRequestCommandHandler($this->recorder, $this->searchRepository, $this->requestRepository);

        $exception = null;
        try {
            $commandHandler->handle($this->command);
        } catch (Exception $exception) {
        }

        $this->assertNull($exception, 'Unexpected exception has been thrown');
    }

    /**
     * Test that we can save an already existing Search Request.
     * And that the count on the search is increased.
     */
    /** @test */
    public function Save_A_Search_Request_That_Already_Exists()
    {
        // Mock Search Entity
        $search = $this->getMock('\Rodw\SearchEngineBundle\Entity\Search');
        $search->expects($this->once())
            ->method('increase');

        // Mock the searchRepository
        $this->searchRepository->expects($this->once())
            ->method('findByQuery')
            ->with($this->equalTo($this->testQueryString))
            ->will($this->returnValue($search));

        // Mock command
        $this->command->expects($this->once())
            ->method('queryString')
            ->will($this->returnValue($this->testQueryString));

        // Make the call to the commandHandler class
        $commandHandler = new SaveSearchRequestCommandHandler($this->recorder, $this->searchRepository, $this->requestRepository);

        $exception = null;
        try {
            $commandHandler->handle($this->command);
        } catch (Exception $exception) {
        }

        $this->assertNull($exception, 'Unexpected exception has been thrown');
    }
}
