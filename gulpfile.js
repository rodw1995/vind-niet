var gulp = require('gulp');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var concat = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');
var del = require('del');
var rename = require("gulp-rename");
var replace = require('gulp-replace');

var fingerPrint = Math.random().toString(36).substring(7);
var dist = 'public_html';
var src = 'resources';

gulp.task('js-clean', function () {
    return del([dist + '/js/all_*.js']);
});

gulp.task('js', ['js-clean'], function() {
    return gulp.src([
        src + '/js/jquery-1.11.3.min.js',
        src + '/js/angular.min.js',
        src + '/js/angular-route.min.js',
        src + '/js/angular-sanitize.min.js',
        src + '/js/bootstrap.min.js',
        src + '/js/script.js'])
        .pipe(concat('all_' + fingerPrint + '.js'))
        .pipe(uglify({'mangle' : false}))
        .pipe(gulp.dest(dist + '/js'));
});

gulp.task('copy-fonts', function () {
    return gulp.src(src + '/fonts/**')
        .pipe(gulp.dest(dist + '/fonts'));
});

gulp.task('css-clean', function () {
    return del([dist + '/css/all_*.css']);
});

gulp.task('css', ['css-clean', 'copy-fonts'], function() {
    return gulp.src([
        src + '/css/bootstrap.min.css',
        src + '/css/style.css'])
        .pipe(concat('all_' + fingerPrint + '.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest(dist + '/css'));
});

gulp.task('api-html', function() {
    return gulp.src('app/Resources/NelmioApiDocBundle/views/layout.html.twig')
        .pipe(replace(/all_(.+?(?=\.))/g, 'all_' + fingerPrint))
        .pipe(gulp.dest('app/Resources/NelmioApiDocBundle/views'))
});

gulp.task('html', ['js', 'css', 'api-html'], function() {
    return gulp.src(src + '/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(replace('all.css', 'all_' + fingerPrint + '.css'))
        .pipe(replace('all.js', 'all_' + fingerPrint + '.js'))
        .pipe(gulp.dest(dist))
});

gulp.task('default', ['html']);