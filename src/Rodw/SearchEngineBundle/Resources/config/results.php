<?php

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// WATCH OUT! Here be dragons.
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
/*
                                                  .~))>>
                                                 .~)>>
                                               .~))))>>>
                                             .~))>>             ___
                                           .~))>>)))>>      .-~))>>
                                         .~)))))>>       .-~))>>)>
                                       .~)))>>))))>>  .-~)>>)>
                   )                 .~))>>))))>>  .-~)))))>>)>
                ( )@@*)             //)>))))))  .-~))))>>)>
              ).@(@@               //))>>))) .-~))>>)))))>>)>
            (( @.@).              //))))) .-~)>>)))))>>)>
          ))  )@@*.@@ )          //)>))) //))))))>>))))>>)>
       ((  ((@@@.@@             |/))))) //)))))>>)))>>)>
      )) @@*. )@@ )   (\_(\-\b  |))>)) //)))>>)))))))>>)>
    (( @@@(.@(@ .    _/`-`  ~|b |>))) //)>>)))))))>>)>
     )* @@@ )@*     (@)  (@) /\b|))) //))))))>>))))>>
   (( @. )@( @ .   _/  /    /  \b)) //))>>)))))>>>_._
    )@@ (@@*)@@.  (6///6)- / ^  \b)//))))))>>)))>>   ~~-.
 ( @jgs@@. @@@.*@_ VvvvvV//  ^  \b/)>>))))>>      _.     `bb
  ((@@ @@@*.(@@ . - | o |' \ (  ^   \b)))>>        .'       b`,
   ((@@).*@@ )@ )   \^^^/  ((   ^  ~)_        \  /           b `,
     (@@. (@@ ).     `-'   (((   ^    `\ \ \ \ \|             b  `.
       (*.@*              / ((((        \| | |  \       .       b `.
                         / / (((((  \    \ /  _.-~\     Y,      b  ;
                        / / / (((((( \    \.-~   _.`" _.-~`,    b  ;
                       /   /   `(((((()    )    (((((~      `,  b  ;
                     _/  _/      `"""/   /'                  ; b   ;
                 _.-~_.-~           /  /'                _.'~bb _.'
               ((((~~              / /'              _.'~bb.--~
                                  ((((          __.-~bb.-~
                                              .'  b .~~
                                              :bb ,'
                                              ~~~~
*/

return [

    'pindakaas gegeten' => [
        [
            'title'         => 'Rapper Sjors - Ik heb net pindakaas gegeten',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/r3KVpo_8HOU?rel=0&autoplay=1&iv_load_policy=3'
        ]
    ],

    'ik houd van json' => [
        [
            'title'         => 'Ik ook',
            'type'          => 'text',
            'raw_output'    => 'Daarnaast houd ik ook van pindakaas, lampen en deuren die open gaan.'
        ]
    ],

    'draadstaal overload' => [
        [
            'title'         => 'Medische Fouten',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/2SohQo6pMEA?rel=0&autoplay=1&iv_load_policy=3',
        ],
        [
            'title'         => 'Fred ergert zich aan irritatie',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/Gx0l4rn2zIU?rel=0&autoplay=0&iv_load_policy=3',
        ],
        [
            'title'         => 'Overval van Joop',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/AgmE3hQHR14?rel=0&autoplay=0&iv_load_policy=3',
        ],
        [
            'title'         => 'Versiertrucs in \'t Brabants',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/duLD_kA8mnI?rel=0&autoplay=0&iv_load_policy=3',
        ],
        [
            'title'         => 'Sayid Computer',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/a1UvNizYr-c?rel=0&autoplay=0&iv_load_policy=3',
        ],
        [
            'title'         => 'The AgainMen',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/4fp_-FB_nvg?rel=0&autoplay=0&iv_load_policy=3',
        ],
    ],

    'lingo overload' => [
        [
            'title'         => 'ABCDEFG',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/8lfYNgTd73I?rel=0&autoplay=1&iv_load_policy=3'
        ],
        [
            'title'         => 'Teamgenoot faalt',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/fLJxB8YOCKk?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Lingo faal team',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/CW-CcNXJaA4?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Debrileaude',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/pNNHcCsppR8?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Blij zijn',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/trzOZL97_yU?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Hidde heeft honger',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/DTEXEkEAI98?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Mevrouw is een gelegenheidsroker',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/TB5uZolEEwM?rel=0&autoplay=0&iv_load_policy=3'
        ],
    ],

    'mand' => [
        [
            'title'         => 'MAND!',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/ypSKDY4Jp3g?rel=0&autoplay=1&iv_load_policy=3'
        ]
    ],

    'ben ik knap?' => [
        [
            'title'         => 'Nee',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/m54gjpJ04pk?rel=0&autoplay=1&iv_load_policy=3'
        ]
    ],

    '#' => [
        [
            'title'         => 'Uh, wat?',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/CU83bmwMiYU?rel=0&autoplay=1&iv_load_policy=3'
        ]
    ],

    'python' => [
        [
            'title'         => 'Annabel',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/THZhnpuV9jc?rel=0&autoplay=1&iv_load_policy=3'
        ]
    ],

    'boris van der ham' => [
        [
            'title'         => 'Binneland 1',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/yO1h6PPF1MA?rel=0&autoplay=1&iv_load_policy=3'
        ]
    ],

    'binnenland 1 overload' => [
        [
            'title'         => 'Binneland 1 - Aflevering 1',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/tippJc8ZhAQ?rel=0&autoplay=1&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 2',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/yO1h6PPF1MA?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 3',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/K6zm4v_77dU?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 4',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/3SyynPc_u7g?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 5',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/84mbAqfC0HY?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 6',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/um-lrVARqwQ?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 7',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/wANaHPPWH3A?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 8',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/zmUuaQs40bc?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 9',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/BAdm_i8oUGw?rel=0&autoplay=0&iv_load_policy=3'
        ],
        [
            'title'         => 'Binneland 1 - Aflevering 10',
            'type'          => 'youtube',
            'raw_output'    => '//www.youtube.com/embed/5Cqm9vSCQdE?rel=0&autoplay=0&iv_load_policy=3'
        ],
    ],
];
