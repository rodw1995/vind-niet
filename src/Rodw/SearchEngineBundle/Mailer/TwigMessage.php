<?php

namespace Rodw\SearchEngineBundle\Mailer;


use Hip\MandrillBundle\Message;
use Symfony\Bundle\TwigBundle\TwigEngine;

class TwigMessage extends Message
{
    /**
     * @var TwigEngine
     */
    protected $twigEngine;

    /**
     * @var string
     */
    protected $templateName;

    /**
     * @var array
     */
    protected $arguments;

    /**
     * @param TwigEngine $twigEngine
     * @param $templateName
     */
    public function __construct(TwigEngine $twigEngine, $templateName)
    {
        $this->twigEngine = $twigEngine;
        $this->templateName = $templateName;
    }

    /**
     * Set the arguments that can be used in placeholders and in the twig template
     *
     * @param array $arguments
     */
    public function setArguments(array $arguments)
    {
        $this->arguments = $arguments;
    }

    /**
     * Set HTML content to the twig template
     *
     * @return Message
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function render()
    {
        $html = $this->twigEngine->render($this->templateName, $this->arguments);

        $this->setText($this->replacePlaceholdersWithArguments($this->getText()));
        $this->setSubject($this->replacePlaceholdersWithArguments($this->getSubject()));

        return $this->setHtml($html);
    }

    /**
     * Replace placeholders in a string with the corresponding argument
     *
     * @param $placeholderString
     * @return string
     */
    public function replacePlaceholdersWithArguments($placeholderString)
    {
        $arguments = $this->arguments;

        return preg_replace_callback('/{~(.*?)~}/', function ($matches) use ($arguments) {
            if (isset($arguments[$matches[1]]))
            {
                return $arguments[$matches[1]];
            }

            return $matches[1];
        }, $placeholderString);
    }
}