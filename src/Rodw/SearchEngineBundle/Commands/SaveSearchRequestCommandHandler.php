<?php

namespace Rodw\SearchEngineBundle\Commands;

use Rodw\SearchEngineBundle\Entity\Request;
use Rodw\SearchEngineBundle\Entity\Search;
use Rodw\SearchEngineBundle\Events\SearchRequestWasPosted;
use Rodw\SearchEngineBundle\Repository\Contracts\RequestRepositoryInterface;
use Rodw\SearchEngineBundle\Repository\Contracts\SearchRepositoryInterface;
use SimpleBus\Message\Recorder\RecordsMessages;

class SaveSearchRequestCommandHandler
{
    /**
     * @var SearchRepositoryInterface
     */
    private $searchRepository;

    /**
     * @var RequestRepositoryInterface
     */
    private $requestRepository;

    /**
     * @var RecordsMessages
     */
    private $eventRecorder;

    /**
     * @param RecordsMessages $eventRecorder
     * @param SearchRepositoryInterface $searchRepository
     * @param RequestRepositoryInterface $requestRepository
     */
    public function __construct(RecordsMessages $eventRecorder,
                                SearchRepositoryInterface $searchRepository, RequestRepositoryInterface $requestRepository)
    {
        $this->searchRepository = $searchRepository;
        $this->requestRepository = $requestRepository;
        $this->eventRecorder = $eventRecorder;
    }

    /**
     * Update the search and request objects
     *
     * @param $command
     */
    public function handle(SaveSearchRequestCommand $command)
    {
        $search = $this->searchRepository->findByQuery($command->queryString());

        if (!$search) {
            // Create a new search object
            $search = new Search();
            $search->setQuery($command->queryString());
        }

        $search->increase();

        // Create the new search request
        $searchRequest = new Request();
        $searchRequest->setSearch($search);

        // Save the changes to the database
        $this->searchRepository->save($search);
        $this->requestRepository->save($searchRequest);

        // Raise the SearchRequestWasPosted event
        $event = new SearchRequestWasPosted($searchRequest);

        $this->eventRecorder->record($event);
    }
}