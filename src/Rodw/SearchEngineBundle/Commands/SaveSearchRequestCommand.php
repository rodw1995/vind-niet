<?php

namespace Rodw\SearchEngineBundle\Commands;

class SaveSearchRequestCommand
{
    /**
     * @var String
     */
    private $queryString;

    /**
     * @param $queryString
     */
    public function __construct($queryString)
    {
        $this->queryString = $queryString;
    }

    /**
     * Get the queryString
     *
     * @return String
     */
    public function queryString()
    {
        return $this->queryString;
    }
}