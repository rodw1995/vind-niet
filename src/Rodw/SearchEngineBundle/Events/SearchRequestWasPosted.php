<?php

namespace Rodw\SearchEngineBundle\Events;


use Rodw\SearchEngineBundle\Entity\Request;

class SearchRequestWasPosted
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the request entity
     *
     * @return Request
     */
    public function request()
    {
        return $this->request;
    }

    /**
     * Shortcut to get the search object
     *
     * @return \Rodw\SearchEngineBundle\Entity\Search
     */
    public function search()
    {
        return $this->request()->getSearch();
    }
}