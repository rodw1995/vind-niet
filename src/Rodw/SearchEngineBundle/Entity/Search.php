<?php

namespace Rodw\SearchEngineBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * Search
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rodw\SearchEngineBundle\Repository\Doctrine\ORM\SearchRepository")
 * */
class Search
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Exclude
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="query", type="string", length=255)
     */
    private $query;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @ORM\OneToMany(targetEntity="Request", mappedBy="search")
     */
    protected $requests;

    public function __construct()
    {
        $this->requests = new ArrayCollection();
        $this->amount = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set query
     *
     * @param string $query
     * @return Search
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query
     *
     * @return string 
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Search
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Increase the amount of the search
     *
     * @return $this
     */
    public function increase()
    {
        $this->setAmount($this->getAmount() + 1);

        return $this;
    }

    /**
     * Add requests
     *
     * @param \Rodw\SearchEngineBundle\Entity\Request $requests
     * @return Search
     */
    public function addRequest(\Rodw\SearchEngineBundle\Entity\Request $requests)
    {
        $this->requests[] = $requests;

        return $this;
    }

    /**
     * Remove requests
     *
     * @param \Rodw\SearchEngineBundle\Entity\Request $requests
     */
    public function removeRequest(\Rodw\SearchEngineBundle\Entity\Request $requests)
    {
        $this->requests->removeElement($requests);
    }

    /**
     * Get requests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * Get the information of the entity in an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'search_id' => $this->id,
            'search_amount' => $this->amount,
            'search_query' => $this->query,
        ];
    }
}
