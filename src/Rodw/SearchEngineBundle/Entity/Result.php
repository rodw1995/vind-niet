<?php

namespace Rodw\SearchEngineBundle\Entity;


class Result
{
    /**
     * The title of the result
     *
     * @var String
     */
    protected $title;

    /**
     * The type of the result
     *
     * @var String youtube|text
     */
    protected $type;

    /**
     * Minimized output
     *
     * @var string
     */
    protected $raw_output;

    /**
     * Output that is ready for printing on the screen
     *
     * @var mixed
     */
    protected $output;

    /**
     * @param $title
     * @param $type
     * @param $raw_output
     * @param $output
     */
    public function __construct($title, $type, $raw_output, $output)
    {
        $this->title = $title;
        $this->type = $type;
        $this->raw_output = $raw_output;
        $this->output = $output;
    }

    /**
     * Get the raw output
     *
     * @return string
     */
    public function rawOutput()
    {
        return $this->raw_output;
    }

    /**
     * Get the full output
     *
     * @return mixed
     */
    public function output()
    {
        return $this->output;
    }

    /**
     * Get the type of the result
     *
     * @return String
     */
    public function type()
    {
        return $this->type;
    }

    /**
     * Determine if the result is of a given type
     *
     * @param $type
     * @return bool
     */
    public function isOfType($type)
    {
        return $this->type() == $type;
    }
}