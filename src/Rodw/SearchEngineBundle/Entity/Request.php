<?php

namespace Rodw\SearchEngineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * Request
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rodw\SearchEngineBundle\Repository\Doctrine\ORM\RequestRepository")
 */
class Request
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Exclude
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity="Search", inversedBy="requests")
     * @ORM\JoinColumn(name="search_id", referencedColumnName="id")
     */
    protected $search;

    public function __construct()
    {
        $this->datetime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Request
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set search
     *
     * @param \Rodw\SearchEngineBundle\Entity\Search $search
     * @return Request
     */
    public function setSearch(\Rodw\SearchEngineBundle\Entity\Search $search = null)
    {
        $this->search = $search;

        return $this;
    }

    /**
     * Get search
     *
     * @return \Rodw\SearchEngineBundle\Entity\Search 
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * Get the information of the entity in an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'request_id' => $this->id,
            'request_datetime' => $this->datetime,
        ];
    }
}
