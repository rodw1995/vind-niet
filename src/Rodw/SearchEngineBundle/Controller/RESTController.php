<?php

namespace Rodw\SearchEngineBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Rodw\SearchEngineBundle\Commands\SaveSearchRequestCommand;
use Rodw\SearchEngineBundle\Entity\Search;
use Rodw\SearchEngineBundle\Repository\Contracts\GambleRepositoryInterface;
use Rodw\SearchEngineBundle\Repository\Contracts\RequestRepositoryInterface;
use Rodw\SearchEngineBundle\Repository\Contracts\ResultRepositoryInterface;
use Rodw\SearchEngineBundle\Repository\Contracts\SearchRepositoryInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RESTController
{
    /**
     * @var SearchRepositoryInterface
     */
    private $searchRepository;

    /**
     * @var RequestRepositoryInterface
     */
    private $requestRepository;

    /**
     * @var ResultRepositoryInterface
     */
    private $resultRepository;

    /**
     * @var GambleRepositoryInterface
     */
    private $gambleRepository;

    /**
     * @var MessageBus
     */
    private $commandBus;

    public function __construct(SearchRepositoryInterface $searchRepository, RequestRepositoryInterface $requestRepository,
                                ResultRepositoryInterface $resultRepository, GambleRepositoryInterface $gambleRepository, MessageBus $commandBus)
    {
        $this->searchRepository = $searchRepository;
        $this->requestRepository = $requestRepository;
        $this->resultRepository = $resultRepository;
        $this->gambleRepository = $gambleRepository;
        $this->commandBus = $commandBus;
    }

    /**
     * Get the results for a given search string. DEPRECATED, use POST api/searches instead
     *
     * @ApiDoc(
     * requirements={
     *      {
     *          "name"="_format",
     *          "dataType"="string",
     *          "requirement"="json|xml",
     *          "description"="Response format",
     *      },
     *      {
     *          "name"="queryString",
     *          "dataType"="string",
     *          "requirement"="double urlencoded base64 string (urlencode(urlencode(base64_encode($queryString)))",
     *          "description"="Search string"
     *      }
     *  },
     * deprecated = true,
     * )
     *
     * @param $queryString // Encoding of the string happens in the RESTControllerListener
     * @return array
     * @View()
     */
    public function getResultsAction($queryString)
    {
        // Find all results for this queryString
        $results = $this->resultRepository->all($queryString);

        return ['results' => $results];
    }

    /**
     * Get all search requests
     *
     * @ApiDoc(
     * requirements={
     *      {
     *          "name"="_format",
     *          "dataType"="string",
     *          "requirement"="json|xml",
     *          "description"="Response format"
     *      }
     *  },
     * )
     *
     * @return array
     * @View()
     */
    public function getSearchesAction()
    {
        $searches = $this->requestRepository->findAllOrderByDate('DESC');

        return ['searches' => $searches];
    }

    /**
     * Post a new search request
     *
     * @ApiDoc(
     * parameters={
     *      {
     *          "name"="queryString",
     *          "dataType"="string",
     *          "format"="non-empty string",
     *          "required"=true,
     *          "description"="Search string"
     *      }
     *  },
     * requirements={
     *      {
     *          "name"="_format",
     *          "dataType"="string",
     *          "requirement"="json|xml",
     *          "description"="Response format"
     *      }
     *  },
     * )
     *
     * @param Request $request
     * @return array
     * @View()
     */
    public function postSearchAction(Request $request)
    {
        $queryString = $request->request->get('queryString', '');

        // Check if the queryString is not empty
        if (!empty($queryString)) {

            $command = new SaveSearchRequestCommand($queryString);

            $this->commandBus->handle($command);

            return $this->getResultsAction($queryString);
        }

        throw new BadRequestHttpException('You must provide a (non empty) query string');
    }

    /**
     * Retrieve information about a search action
     *
     * @ApiDoc(
     * requirements={
     *      {
     *          "name"="_format",
     *          "dataType"="string",
     *          "requirement"="json|xml",
     *          "description"="Response format"
     *      },
     *      {
     *          "name"="queryString",
     *          "dataType"="string",
     *          "requirement"="double urlencoded base64 string (urlencode(urlencode(base64_encode($queryString)))",
     *          "description"="Search string"
     *      },
     *  },
     * )
     *
     * @param $queryString
     * @return array
     * @View()
     */
    public function getSearchAction($queryString)
    {
        // Get information about searches with this queryString
        $about = $this->searchRepository->findByQuery($queryString);

        if (!$about) {
            // There is never been a search request with this string
            // So create an empty Search object and return that
            $about = new Search();
            $about->setQuery($queryString);
        }

        return ['result' => $about];
    }

    /**
     * Get a random word or sentence
     *
     * @ApiDoc(
     * requirements={
     *      {
     *          "name"="_format",
     *          "dataType"="string",
     *          "requirement"="json|xml",
     *          "description"="Response format"
     *      },
     *  },
     * )
     *
     * @return array
     * @View()
     */
    public function getGambleAction()
    {
        return ['query_string' => $this->gambleRepository->random()];
    }
}
