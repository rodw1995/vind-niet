<?php

namespace Rodw\SearchEngineBundle\Repository\ArrayResource;


use Rodw\SearchEngineBundle\Repository\Contracts\GambleRepositoryInterface;

class GambleRepository implements GambleRepositoryInterface
{
    /**
     * The array that holds all queryStrings
     *
     * @var array
     */
    protected $queryStrings;

    /**
     * @param array $queryStrings
     */
    public function __construct(array $queryStrings)
    {
        $this->queryStrings = $queryStrings;
    }

    /**
     * Return a random queryString
     *
     * @return mixed
     */
    public function random()
    {
        $queryStrings = $this->queryStrings;
        shuffle($queryStrings);

        return array_shift($queryStrings);
    }
}