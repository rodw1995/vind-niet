<?php

namespace Rodw\SearchEngineBundle\Repository\ArrayResource;


use Rodw\SearchEngineBundle\Factory\ResultFactory;
use Rodw\SearchEngineBundle\Repository\Contracts\ResultRepositoryInterface;

class ResultRepository implements ResultRepositoryInterface
{
    /**
     * Array where the results are stored
     *
     * @var array
     */
    protected $results;

    /**
     * Result factory for creating new result objects
     *
     * @var ResultFactory
     */
    protected $factory;

    /**
     * @param ResultFactory $factory
     * @param array $results
     */
    public function __construct(ResultFactory $factory, array $results)
    {
        $this->factory = $factory;
        $this->results = $results;
    }

    /**
     * Determine if a given queryString has any results
     *
     * @param $queryString
     * @return bool
     */
    public function hasResults($queryString)
    {
        return isset($this->results[strtolower($queryString)]);
    }

    /**
     * Return all results by a given queryString
     *
     * @param $queryString
     * @return array
     */
    public function all($queryString)
    {
        $queryString = strtolower($queryString);
        $allResults = [];

        if ($this->hasResults($queryString))
        {
            foreach ($this->results[$queryString] as $result)
            {
                $allResults[] = $this->factory->create($result);
            }
        }

        return $allResults;
    }
}