<?php


namespace Rodw\SearchEngineBundle\Repository\Contracts;



use Rodw\SearchEngineBundle\Entity\Request;

interface RequestRepositoryInterface
{
    /**
     * Save a Request object on the database
     *
     * @param Request $request
     * @return mixed
     */
    public function save(Request $request);

    /**
     * Get all Requests ordered by date
     *
     * @param string $order
     * @return mixed
     */
    public function findAllOrderByDate($order = 'asc');
}