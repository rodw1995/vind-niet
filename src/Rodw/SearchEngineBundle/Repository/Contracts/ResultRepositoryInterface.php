<?php

namespace Rodw\SearchEngineBundle\Repository\Contracts;

interface ResultRepositoryInterface
{
    /**
     * Return all results by a given queryString
     *
     * @param $queryString
     * @return array
     */
    public function all($queryString);

    /**
     * Determine if a given queryString has any results
     *
     * @param $queryString
     * @return bool
     */
    public function hasResults($queryString);
}