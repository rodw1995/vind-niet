<?php


namespace Rodw\SearchEngineBundle\Repository\Contracts;


use Rodw\SearchEngineBundle\Entity\Search;

interface SearchRepositoryInterface
{
    /**
     * Find a search by a queryString
     *
     * @param $queryString
     * @return Search
     */
    public function findByQuery($queryString);

    /**
     * Save a search entity on the database
     *
     * @param Search $search
     * @return mixed
     */
    public function save(Search $search);
}