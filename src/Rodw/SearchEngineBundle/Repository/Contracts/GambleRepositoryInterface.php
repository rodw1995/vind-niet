<?php


namespace Rodw\SearchEngineBundle\Repository\Contracts;


interface GambleRepositoryInterface
{
    public function random();
}