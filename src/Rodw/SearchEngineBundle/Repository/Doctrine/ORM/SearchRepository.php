<?php

namespace Rodw\SearchEngineBundle\Repository\Doctrine\ORM;


use Doctrine\ORM\EntityRepository;
use Rodw\SearchEngineBundle\Entity\Search;
use Rodw\SearchEngineBundle\Repository\Contracts\SearchRepositoryInterface;

class SearchRepository extends EntityRepository implements SearchRepositoryInterface
{
    public function save(Search $search)
    {
        $this->_em->persist($search);
        $this->_em->flush();
    }

    /**
     * Find a search by a queryString
     *
     * @param $queryString
     * @return Search
     */
    public function findByQuery($queryString)
    {
        return $this->findOneBy(['query' => $queryString]);
    }
}