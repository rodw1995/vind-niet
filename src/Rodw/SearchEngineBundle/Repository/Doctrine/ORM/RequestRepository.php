<?php

namespace Rodw\SearchEngineBundle\Repository\Doctrine\ORM;


use Doctrine\ORM\EntityRepository;
use Rodw\SearchEngineBundle\Entity\Request;
use Rodw\SearchEngineBundle\Repository\Contracts\RequestRepositoryInterface;

class RequestRepository extends EntityRepository implements RequestRepositoryInterface
{
    /**
     * Save a Request object on the database
     *
     * @param Request $request
     * @return mixed
     */
    public function save(Request $request)
    {
        $this->_em->persist($request);
        $this->_em->flush();
    }

    /**
     * Get all Requests ordered by date
     *
     * @param string $order
     * @return mixed
     */
    public function findAllOrderByDate($order = 'ASC')
    {
        return $this->findBy([], ['datetime' => $order]);
    }
}