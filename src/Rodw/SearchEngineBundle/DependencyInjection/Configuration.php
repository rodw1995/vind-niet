<?php

namespace Rodw\SearchEngineBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('rodw_search_engine');

        $rootNode
            ->children()
                ->scalarNode('template')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('subject')
                    ->defaultValue('Request for: {~search_query~}')
                ->end()
                ->scalarNode('send_to')
                    ->isRequired()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
