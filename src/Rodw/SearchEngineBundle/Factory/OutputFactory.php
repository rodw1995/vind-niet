<?php


namespace Rodw\SearchEngineBundle\Factory;


interface OutputFactory
{
    public function create($title, $type, $raw_output);
}