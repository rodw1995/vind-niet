<?php

namespace Rodw\SearchEngineBundle\Factory;

use Rodw\SearchEngineBundle\Entity\Result;

class ResultFactory
{
    /**
     * Output Factory that can create our real output
     *
     * @var OutputFactory
     */
    private $outputFactory;

    /**
     * @param OutputFactory $outputFactory
     */
    public function __construct(OutputFactory $outputFactory)
    {
        $this->outputFactory = $outputFactory;
    }

    /**
     * Create a new instance of a result class
     *
     * @param $values
     * @return Result
     */
    public function create($values)
    {
        // extract($values);
        $title = $values['title'];
        $type = $values['type'];
        $raw_output = $values['raw_output'];
        $output = (isset($values['output'])) ? $values['output'] :
            $this->outputFactory->create($title, $type, $raw_output);

        return new Result($title, $type, $raw_output, $output);
    }
}