<?php

namespace Rodw\SearchEngineBundle\Factory;


use Symfony\Bundle\TwigBundle\TwigEngine;

class OutputTwigTemplateFactory implements OutputFactory
{
    /**
     * Reference to the twigEngine
     *
     * @var TwigEngine
     */
    private $twigEngine;

    /**
     * @param TwigEngine $twigEngine
     */
    public function __construct(TwigEngine $twigEngine)
    {
        $this->twigEngine = $twigEngine;
    }

    /**
     * Render a view based on type and return the output
     *
     * @param $title
     * @param $type
     * @param $raw_output
     * @return string
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function create($title, $type, $raw_output)
    {
        $template = "@RodwSearchEngine/Results/Partials/" . $type . ".html.twig";
        return $this->twigEngine->render($template, compact('raw_output', 'title', 'type'));
    }
}