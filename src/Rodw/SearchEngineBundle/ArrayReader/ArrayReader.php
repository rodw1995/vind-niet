<?php

namespace Rodw\SearchEngineBundle\ArrayReader;


class ArrayReader
{
    /**
     * Array that the file returned
     *
     * @var mixed
     */
    protected $array;

    /**
     * @param $filePath
     */
    public function __construct($filePath)
    {
        $this->array = include $filePath;

        if (!is_array($this->array)) {
            $message = 'The file ' . $filePath . ' has to return an array';
            throw new \InvalidArgumentException($message);
        }
    }

    /**
     * Retrieve the array from the file
     *
     * @return mixed
     */
    public function getArray()
    {
        return $this->array;
    }
}