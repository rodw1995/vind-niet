<?php

namespace Rodw\SearchEngineBundle\EventListener;

use Rodw\SearchEngineBundle\Controller\RESTController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class RESTControllerListener
{
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof RESTController)
        {
            $attributes = $event->getRequest()->attributes;

            if ($attributes->has('queryString'))
            {
                // Replace the decoded queryString with an encoded one
                $attributes->replace([
                    'queryString' => base64_decode(urldecode(urldecode($attributes->get('queryString'))))
                ]);
            }
        }
    }
}