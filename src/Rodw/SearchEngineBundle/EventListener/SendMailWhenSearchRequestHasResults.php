<?php

namespace Rodw\SearchEngineBundle\EventListener;


use Hip\MandrillBundle\Dispatcher;
use Hip\MandrillBundle\Message;
use Rodw\SearchEngineBundle\Events\SearchRequestWasPosted;
use Rodw\SearchEngineBundle\Mailer\TwigMessage;
use Rodw\SearchEngineBundle\Repository\Contracts\ResultRepositoryInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;

class SendMailWhenSearchRequestHasResults
{
    /**
     * @var Dispatcher
     */
    private $mailDispatcher;

    /**
     * @var TwigMessage
     */
    private $message;

    /**
     * @var ResultRepositoryInterface
     */
    private $resultRepository;

    /**
     * @param Dispatcher $mailDispatcher
     * @param TwigMessage $message
     * @param ResultRepositoryInterface $resultRepository
     */
    public function __construct(Dispatcher $mailDispatcher, TwigMessage $message, ResultRepositoryInterface $resultRepository)
    {
        $this->mailDispatcher = $mailDispatcher;
        $this->message = $message;
        $this->resultRepository = $resultRepository;
    }

    /**
     * Send a email
     *
     * @param SearchRequestWasPosted $event
     */
    public function notify(SearchRequestWasPosted $event)
    {
        // Only send a mail when there were results
        if ($this->resultRepository->hasResults($event->search()->getQuery())) {
            $this->message->setArguments(array_merge($event->request()->toArray(), $event->search()->toArray()));
            $this->message->render();

            $this->mailDispatcher->send($this->message);
        }
    }
}